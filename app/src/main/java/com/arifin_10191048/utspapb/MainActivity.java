package com.arifin_10191048.utspapb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGetter> datamenu;

    GridLayoutManager gridLayoutManager;

    DashBoardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rlmenu);

        addData();
        gridLayoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter = new DashBoardAdapter(datamenu);
        recyclerView.setAdapter(adapter);
    }

    public void addData(){
        datamenu = new ArrayList<>();
        datamenu.add(new SetterGetter("News Update", "logomenu1"));
        datamenu.add(new SetterGetter("Scholarship", "logomenu2"));
        datamenu.add(new SetterGetter("View", "logomenu3"));
        datamenu.add(new SetterGetter("Partnership", "logomenu4"));
        datamenu.add(new SetterGetter("Facility", "logomenu5"));
        datamenu.add(new SetterGetter("Graduation", "logomenu6"));
        datamenu.add(new SetterGetter("Laboratory", "logomenu7"));
        datamenu.add(new SetterGetter("Faculty", "logomenu8"));
        datamenu.add(new SetterGetter("Lecturer", "logomenu9"));
    }
}