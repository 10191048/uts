package com.arifin_10191048.utspapb;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DashBoardAdapter extends RecyclerView.Adapter<DashBoardAdapter.DashBoardHolder> {

    private ArrayList<SetterGetter> listdata;

    public DashBoardAdapter(ArrayList<SetterGetter> listdata){

        this.listdata = listdata;
    }

    @NonNull
    @Override
    public DashBoardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard,parent,false);
        DashBoardHolder holder = new DashBoardHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DashBoardHolder holder, int position) {
        final SetterGetter getData = listdata.get(position);
        String titlemenu = getData.getTitle();
        String menu = getData.getImg();

        holder.titleMenu.setText(titlemenu);
        if (menu.equals("logomenu1")){
            holder.imgMenu.setImageResource(R.drawable.news);
        } else if (menu.equals("logomenu2")){
            holder.imgMenu.setImageResource(R.drawable.scholar);
        } else if (menu.equals("logomenu3")){
            holder.imgMenu.setImageResource(R.drawable.view);
        } else if (menu.equals("logomenu4")){
            holder.imgMenu.setImageResource(R.drawable.partnership);
        } else if (menu.equals("logomenu5")){
            holder.imgMenu.setImageResource(R.drawable.facility);
        } else if (menu.equals("logomenu6")){
            holder.imgMenu.setImageResource(R.drawable.graduation);
        } else if (menu.equals("logomenu7")){
            holder.imgMenu.setImageResource(R.drawable.laboratory);
        } else if (menu.equals("logomenu8")){
            holder.imgMenu.setImageResource(R.drawable.faculty);
        } else if (menu.equals("logomenu9")){
            holder.imgMenu.setImageResource(R.drawable.lacturer);
        }
    }

    @Override
    public int getItemCount() {

        return listdata.size();
    }

    public class DashBoardHolder extends RecyclerView.ViewHolder {

        TextView titleMenu;
        ImageView imgMenu;

        public DashBoardHolder(@NonNull View itemView) {

            super(itemView);

            titleMenu = itemView.findViewById(R.id.titlemenu);
            imgMenu = itemView.findViewById(R.id.menu);
        }
    }
}
